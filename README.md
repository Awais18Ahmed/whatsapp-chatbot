# WhatsApp Chatbot

# Overview

This project leverages the power of Node.js, Express, and the WhatsApp Cloud API with GraphQL provided by Meta (formerly Facebook) to create a robust and interactive WhatsApp chat bot. The bot is designed to facilitate seamless communication by enabling users to engage with predefined templates.

# Key Features

## Template-Driven Interaction:   
Users can effortlessly interact with the chat bot using intuitively designed templates, streamlining common tasks and enhancing user experience.  

## GraphQL Integration with Meta API:   
Harnesses the capabilities of GraphQL to seamlessly integrate with the WhatsApp Cloud API from Meta, ensuring efficient and reliable message processing on WhatsApp.  

## Node.js and Express Backend:   
A scalable and high-performance backend implemented in Node.js and Express, handling incoming requests, processing messages, and managing user interactions.  


# Install Dependencies:
To make it easy for you to get started with GitLab, here's a list of recommended next steps.   
cd whatsapp-chat-bot  
npm install  

# Run the Application:  
npm start

